package com.company;

import java.util.HashMap;
import java.util.Map;

public class Bank implements BankOperation {

    private Map<Integer, Integer> accounts;
    private int lastAccountNumber;

    Bank() {
        accounts = new HashMap<>();
        lastAccountNumber = 0;
    }

    @Override
    public int createAccount() {
        accounts.put(++lastAccountNumber,0);
        return lastAccountNumber;
    }

    @Override
    public int deleteAccount(int accountNumber) {
        if(accounts.containsKey(accountNumber)) {
            int accountBalance = accounts.get(accountNumber);
            accounts.remove(accountNumber);
            return accountBalance;
        }
        return ACCOUNT_NOT_EXISTS;
    }

    @Override
    public boolean deposit(int accountNumber, int amount) {
        if(accounts.containsKey(accountNumber) && amount>0) {
            accounts.put(accountNumber, accounts.get(accountNumber) + amount);
            return true;
        }
        return false;
    }

    @Override
    public boolean withdraw(int accountNumber, int amount) {
        if(accounts.containsKey(accountNumber) && amount>0 && accounts.get(accountNumber)>=amount) {
            accounts.put(accountNumber,accounts.get(accountNumber)-amount);
            return true;
        }
        return false;
    }

    @Override
    public boolean transfer(int fromAccount, int toAccount, int amount) {
        if(accounts.containsKey(fromAccount) && accounts.containsKey(toAccount) && amount>0 && accounts.get(fromAccount)>=amount) {
            accounts.put(fromAccount,accounts.get(fromAccount)-amount);
            accounts.put(toAccount,accounts.get(toAccount)+amount);
            return true;
        }
        return false;
    }

    @Override
    public int accountBalance(int accountNumber) {
        if(accounts.containsKey(accountNumber)){
            return accounts.get(accountNumber);
        }
        return ACCOUNT_NOT_EXISTS;
    }

    @Override
    public int sumAccountsBalance() {
        return accounts.entrySet().stream().mapToInt(i ->i.getValue()).sum();
    }
}
